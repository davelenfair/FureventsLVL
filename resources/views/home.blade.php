@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3 p-3">
        <!-- Deze afbeelding is een placeholder voor de profielfoto -->
            <img src="http://d.facdn.net/art/ticothetimberwolf/1578495605/1578495605.ticothetimberwolf_tico_fursuit-1.jpg" width="128" height="128" class="rounded-circle">
        </div>
        <div class="col-9">
        <div><h1>{{ $user->gebruikersnaam }}</h1></div>
            <div class="d-flex">
            <!-- Nogmaals, placeholders. Houden nog geen variabelen -->
                <div class="pr-5"><strong>Statistieken:</strong></div>
                <div class="pr-3"><strong>1</strong> Actief event</div>
                <div class="pr-3"><strong>2</strong> Gearchiveerde events</div>
            </div>
            <!-- Overzicht events gebruiker, nog geen varabelen -->
            <div class="pt-5"><h2><strong>laatste 3 events</strong></h2></div>
            <div class="pt-5"><a href="#"><h4><strong>Furwalk Astad</strong></h4></a></div>
            <div><p>Beschrijving furevent</p></div>
            <div class="pt-5"><a href="#"><h4><strong>Furmeet Bdorp</strong></h4></a></div>
            <div><p>Een furmeet in Bdorp</p></div>
            <div class="pt-5"><a href="#"><h4><strong>Furmeet Elfia Arcen</strong></h4></a></div>
        </div>
    </div>
</div>
@endsection
